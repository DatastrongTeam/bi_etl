bi\_etl.parameters package
==========================

Submodules
----------

.. toctree::
   :maxdepth: 4

   bi_etl.parameters.file_parameter

Module contents
---------------

.. automodule:: bi_etl.parameters
   :members:
   :undoc-members:
   :show-inheritance:
